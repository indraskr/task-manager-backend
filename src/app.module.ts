import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as Joi from 'joi';
import { AuthModule } from './auth/auth.module';
import { __prod } from './constants';
import { TasksModule } from './tasks/tasks.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        DB_HOST: Joi.required(),
        DB_USER: Joi.required(),
        DB_PASS: Joi.required(),
        DB_NAME: Joi.required(),
        DB_PORT: Joi.required().default(5432),
        JWT_SECRET: Joi.required(),
      }),
      ignoreEnvFile: __prod,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      autoLoadEntities: true,
      synchronize: !__prod,
    }),
    TasksModule,
    AuthModule,
  ],
})
export class AppModule {}
